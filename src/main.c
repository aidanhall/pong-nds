#include "main.h"

/* Format: up key, down key */
int inputmap[][2] = {
    {KEY_UP, KEY_DOWN},
    {KEY_Y, KEY_B},
};

u32 randVSpeed() {
    return divf32(inttof32(rand()%400), inttof32(400));
}

void consoleReset(void) {
    /* Console */
    consoleDemoInit();
    consoleClear();
    iprintf("pong\n");
    iprintf("Copyright Aidan Hall, 2021\n");
    iprintf("Press start to start. Ha.\n");
}

int main(void) {
    /* Video */
    videoSetMode(MODE_0_2D);

    /* VRAM */
    vramSetBankA(VRAM_A_MAIN_SPRITE);
    /* 2D Sprite Engine */
    oamInit(&oamMain, SpriteMapping_1D_32, false);

    SPRITE_PALETTE[1] = RGB15(31,31,31);
    struct GameObject *paddles[2];
    paddles[0] = initGameObject(inttof32(PADDLE_MARGIN), inttof32(SCREEN_HEIGHT/2), PADDLE_WIDTH, PADDLE_HEIGHT, PADDLE_SPRITE_SIZE);
    paddles[1] = initGameObject(inttof32(SCREEN_WIDTH-PADDLE_MARGIN-PADDLE_WIDTH),
	    inttof32(SCREEN_HEIGHT/2), PADDLE_WIDTH, PADDLE_HEIGHT, PADDLE_SPRITE_SIZE);

    struct GameObject *ball = initGameObject(inttof32(SCREEN_WIDTH/2), inttof32(SCREEN_HEIGHT/2),
	    BALL_DIAMETER, BALL_DIAMETER, BALL_SPRITE_SIZE);

    /* Keyboard */
    keyboardDemoInit();

    /* iprintf("Paddle 0: %d %d\n", paddles[0]->x, paddles[0]->y); */
    consoleReset();

    
    s32 vx = 0,
	vy = 0;

    vx = -floattof32(2.0f);
    vy = randVSpeed();
    int score = 0;
    int angle = 0;
    while (1){
	scanKeys();
	int held = keysHeld();

	/* reset */
	if ((ball->x + inttof32(ball->w)) >= inttof32(SCREEN_WIDTH)
		|| (ball->x <= 0)
		|| (held & KEY_START)
		) {
	    ball->x = inttof32(SCREEN_WIDTH/2);
	    ball->y = paddles[0]->y = paddles[1]->y = inttof32(SCREEN_HEIGHT/2);
	    vx = floattof32(2.0f);
	    vy = randVSpeed();
	    score = 0;
	    consoleReset();
	}

	ball->x += vx;
	ball->y += vy;
	/* oamRotateScale(&oamMain, ball->rot_id, angle, (1<<8),(1<<8)); */
	/* angle += vy * 64; */

	/* collide with paddles */
	if ((ball->x >= paddles[1]->x &&
		    ((ball->y+inttof32(ball->h) >= (paddles[1]->y) && (ball->y+inttof32(ball->h)) <= (paddles[1]->y+inttof32(paddles[1]->h)))||
		     (ball->y <= paddles[1]->y+inttof32(paddles[1]->h) && ball->y >= paddles[1]->y+inttof32(paddles[1]->h))))||
		(ball->x <= paddles[0]->x + inttof32(paddles[0]->w)
		 && ((ball->y+inttof32(ball->h) >= (paddles[0]->y)&& (ball->y) <= (paddles[0]->y+inttof32(paddles[0]->h)))||
		     ((ball->y+inttof32(ball->h) >= (paddles[0]->y) && (ball->y+inttof32(ball->h)<=(paddles[0]->y+inttof32(paddles[0]->h)))))))) {
	    /* iprintf("collide\n"); */

	    vx = mulf32(vx, BALL_X_V_INC_f);

	    if ((ball->y > inttof32(2*SCREEN_WIDTH/3) && vy >= inttof32(0))) {
		vy = -randVSpeed();
	    } else if (ball->y < inttof32(SCREEN_WIDTH/3) && vy < inttof32(0)) {
		vy = randVSpeed();
	    }
	    score++;
	    iprintf("Score: %d\n", score);
	}

	/* collide with top and bottom */
	if (ball->y <= 0 && vy <= 0) {
	    ball->y = 1;
	    vy = randVSpeed();
	} else if (ball->y + inttof32(ball->h) >= inttof32(SCREEN_HEIGHT) && vy >= 0) {
	    vy = -randVSpeed();
	    ball->y = inttof32(SCREEN_HEIGHT) - inttof32(ball->h) - 1;
	}
	for (int i = 0; i < 2; ++i) {
	    if (held & inputmap[i][0]) {
		paddles[i]->y -= inttof32(PAD_SPEED);
	    }
	    if (held & inputmap[i][1]) {
		paddles[i]->y += inttof32(PAD_SPEED);
	    }
	    drawGameObject(paddles[i]);
	}
	drawGameObject(ball);

	swiWaitForVBlank();
	oamUpdate(&oamMain);
    }
    free(paddles[0]);
    free(paddles[1]);
    free(ball);
    return 0;
}

struct GameObject* initGameObject(s32 x, s32 y, u32 w, u32 h, int size) {
    static int id = 0;
    struct GameObject* obj = malloc(sizeof(struct GameObject));
    obj->x = x;
    obj->y = y;
    obj->w = w;
    obj->h = h;
    obj->id = id++;
    obj->rot_id = id++;
    obj->size = size;
    obj->gfx = oamAllocateGfx(&oamMain, size, PAD_COL_FORM);
    memset(obj->gfx, 1|(1<<8), (w*h/2)*sizeof(u16));

    return obj;
}

void drawGameObject(struct GameObject* obj) {
    oamSet(&oamMain,
	    obj->id,
	    /* obj->x, obj->y, */
	    f32toint(obj->x), f32toint(obj->y),
	    0,
	    0,
	    obj->size,
	    PAD_COL_FORM,
	    obj->gfx,
	    /* obj->rot_id, */ -1,
	    true,
	    false,
	    false, false,
	    false);
}
