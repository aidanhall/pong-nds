#pragma once

#include <nds.h>
#include <math.h>
#include <nds/arm9/console.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define PADDLE_SPRITE_SIZE (SpriteSize_8x32)
#define PADDLE_WIDTH (8)
#define PADDLE_HEIGHT (32)
#define PAD_COL_FORM (SpriteColorFormat_256Color)
#define PADDLE_MARGIN (10)

#define BALL_MAX_Y_V (floattof32(0.5f))
#define BALL_X_V_INC_f (floattof32(-1.05f))


#define PAD_SPEED (4)

#define BALL_DIAMETER (8)
#define BALL_SPRITE_SIZE (SpriteSize_8x8)

#define LENGTH(X) ((sizeof(X))/(sizeof(X[0])))

struct GameObject {
    u16 *gfx;
    s32 x,y;
    u32 w,h;
    int size;
    int id;
    int rot_id;
};

struct GameObject* initGameObject(s32 x, s32 y, u32 w, u32 h, int size);
void drawGameObject(struct GameObject* obj);
// vi: ft=c
